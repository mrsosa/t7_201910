package model.data_structures;

import java.util.Iterator;

import junit.framework.TestCase;
/**
 * Test que prueba la estructura de arbol rojo-negro
 * @author mrSosa
 */
public class TestRedBlackBST extends TestCase{
	/**
	 * Arbol que representa un arbol rojo negro con texto 
	 */
	private RedBlackBST<Integer, String> texto;
	/**
	 * Arbol que representa un arbol rojo negro con numeros
	 */
	private RedBlackBST<String, Integer> numero;
	/**
	 * Crea un escenario de pruebas
	 */
	public void setUp(){
		texto = new RedBlackBST<Integer, String>();
		numero = new RedBlackBST<String, Integer>();
	}
	/**
	 * Crea un escenario de pruebas
	 */
	public void setUp1(){
		setUp();

		texto.put(1, "a");
		texto.put(2, "b");
		texto.put(3, "c");
		texto.put(4, "d");
		texto.put(5, "e");

		numero.put("a", 1);
		numero.put("b", 2);
		numero.put("c", 3);
		numero.put("d", 4);
		numero.put("e", 5);
	}
	/**
	 * Crea un escenario de pruebas
	 */
	public void setUp2(){
		setUp();

		texto.put(5, "e");
		texto.put(4, "d");
		texto.put(3, "c");
		texto.put(2, "b");
		texto.put(1, "a");

		numero.put("e", 5);
		numero.put("d", 4);
		numero.put("c", 3);
		numero.put("b", 2);
		numero.put("a", 1);
	}
	/**
	 * Crea un escenario de pruebas
	 */
	public void setUp3(){
		setUp();

		texto.put(3, "c");
		texto.put(1, "a");
		texto.put(4, "d");
		texto.put(2, "b");
		texto.put(5, "e");

		numero.put("c", 3);
		numero.put("a", 1);
		numero.put("d", 4);
		numero.put("b", 2);
		numero.put("e", 5);
	}
	/**
	 * Prueba que el arbol cumpla la definicion de un arbol rojo negro
	 */
	public void testDefinicion() {
		setUp1();
		assertEquals("No cumple con la definicion de una arbol rojo negro", texto.check() ,true );
		assertEquals("No cumple con la definicion de una arbol rojo negro", numero.check() ,true );

		setUp2();
		assertEquals("No cumple con la definicion de una arbol rojo negro", texto.check() ,true );
		assertEquals("No cumple con la definicion de una arbol rojo negro", numero.check() ,true );

		setUp3();
		assertEquals("No cumple con la definicion de una arbol rojo negro", texto.check() ,true );
		assertEquals("No cumple con la definicion de una arbol rojo negro", numero.check() ,true );
	}
	/**
	 * Prueba el metodo put del arbol
	 */
	public void testAgregar(){
		setUp();
		texto.put(11, "j");
		numero.put("q", 11);		
		assertEquals("No se agrego correctamente", texto.size(), 1);
		assertEquals("No se agrego correctamente", numero.size(), 1);

		texto.put(12, "q");
		assertEquals("Deberia existir esta llave en el arbol", texto.contains(12), true);

		numero.put("q", 12);
		assertEquals("Debio hacer cambiado el valor de la llave", numero.get("q") == 12, true);

	}
	/**
	 * Prueba el metodo get del arbol
	 */
	public void testGet(){
		setUp2();
		texto.put(1, "asd");
		assertEquals("Deberia ser true",texto.get(1).equals("asd"),true);
	}
	/**
	 * Prueba el metodo delete del arbol
	 */
	public void testEliminar(){
		setUp2();
		numero.delete("b");
		assertEquals("Deberia ser falso",numero.contains("b") ,false);

		numero.deleteMin();
		assertEquals("Deberia ser falso", numero.contains("a"), false);

		numero.deleteMax();
		assertEquals("Deberia ser falso", numero.contains("e"), false);

		assertEquals("El tama�o debio haber cambiado", numero.size(), 2);
	}
	/**
	 * Prueba el metodo Keys del arbol
	 */
	public void testKeys(){
		setUp1();

		Iterator<Integer> x = texto.keys().iterator();
		int i = 1;
		while(x.hasNext())
		{
			if(x.next() != i) assertTrue("", true);
			i++;
		}

	}
	/**
	 * Prueba el metodo isEmpty del arbol.
	 */
	public void testIsEmpty(){
		setUp();
		assertEquals("Deberia estar vacio",numero.isEmpty(),true);
		numero.put("q", 11);		
		assertEquals("Ya hay un elemento", texto.size() == 1, false);
	}
	/**
	 * Prueba el metodo min del arbol
	 */
	public void testMin() {
		setUp3();
		assertEquals("La llave no es correcta", numero.min().equals("a"), true);
		assertEquals("La llave no es correcta", texto.min().equals(1), true);
	}
	/**
	 * Pruea el metodo max del arbol
	 */
	public void testMax() {
		setUp3();
		assertEquals("La llave no es correcta", numero.max().equals("e"), true);
		assertEquals("La llave no es correcta", texto.max().equals(5), true);
	}
	/**
	 * Prueba el metodo size del arbol
	 */
	public void testSize() {
		setUp();
		texto.put(11, "j");
		numero.put("q", 11);		
		assertEquals("No se agrego correctamente", texto.size(), 1);
		assertEquals("No se agrego correctamente", numero.size(), 1);

		numero.deleteMax();
		assertEquals("El tama�o debio haber cambiado", numero.size(), 0);

		setUp1();
		assertEquals("El tama�o debio haber cambiado", numero.size(), 5);
	}
	/**
	 * Prueba el metodo values in range del rango
	 */
	public void testValuesInRange(){
		setUp1();

		Iterator<Integer> x = numero.valueInRange(numero.min(), numero.max()).iterator();
		int i = 1;
		while(x.hasNext()){
			if(x.next() != i) assertTrue("", true);
			i++;
		}

	}
	/**
	 * Prueba el metodo keys in range del rango
	 */
	public void testKeysInRange(){
		setUp1();

		Iterator<Integer> x = texto.keysInRange(texto.min(), texto.max()).iterator();
		int i = 1;
		while(x.hasNext()){
			if(x.next() != i) assertTrue("", true);
			i++;
		}
	}
	/**
	 * Prueba el metodo height
	 */
	public void testHeight()
	{
		setUp1();
		assertEquals("No se calculo la altura correctamente", texto.height(), 2);
		assertEquals("No se calculo la altura correctamente", numero.height(),2);
	}
	/**
	 * Prueba el metodo getHeight()
	 */
	public void testGetHeight()
	{
		setUp1();
		System.out.println(texto.getHeight(5));
		assertEquals("No se calculo la altura correctamente", texto.getHeight(2), 1);
		assertEquals("No se calculo la altura correctamente", numero.getHeight("b"),1);
	}
}
