package controller;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import model.data_structures.ArregloDinamico;
import model.data_structures.IteratorQueue;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;
/**
 * Clase que representa el controlador 
 * @author nicot
 */
public class Controller 
{
	/**
	 *  Interfaz visual del programa .
	 */
	private MovingViolationsManagerView view;
	/**
	 * Lista de meses del programa
	 */
	private String[] listaMes=new String[12];
	/**
	 * numero total de infracciones
	 */
	private int numTotalInfrac;
	/**
	 * Estructura sobre la cual se cargan los datos
	 */
	private RedBlackBST<Integer,VOMovingViolations> arbol;
	/**
	 * Constructor del controlador
	 */
	public Controller() 
	{
		view = new MovingViolationsManagerView();

		listaMes[0]="January";
		listaMes[1]="February";
		listaMes[2]="March";
		listaMes[3]="April";
		listaMes[4]="May";
		listaMes[5]="June";

		arbol = new RedBlackBST<Integer,VOMovingViolations>();

		numTotalInfrac=0;
	}

	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;

		long startTime;
		long endTime;
		long duration;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();
			int nMuestra=0;

			switch(option)
			{
			case 1:

				view.printMessage("Cargando los datos...");
				loadMovingViolations();

				view.printMessage("Se cargaron los datos.");

				view.printMessage("Altura real del arbol red-black: "+arbol.height());
				view.printMessage("Altura promedio del arbol: "+(25-darAlturaPromedio()));
				
				break;

			case 2:

				view.printMessage("ObjectId a consultar:");
				int objectId = sc.nextInt();
				VOMovingViolations infraccion=darInfraccionPorObjectId(objectId);
				System.out.println(infraccion.toString());

				break;


			case 3:

				view.printMessage("Ingrese el objectId menor del rango a buscar:");
				int objectIdmenor=sc.nextInt();
				view.printMessage("Ingrese el objectId Mayor del rango a buscar:");
				int objectIdMayor=sc.nextInt();
				imprimirPorRangoObjectsId(objectIdmenor,objectIdMayor);
				break;


			case 4:

				fin=true;
				sc.close();
				break;
			}
		}

	}
	/**
	 * 	Carga los csv del cuatrimestre. 
	 */
	public void loadMovingViolations() 
	{		
		int numDatos=0;
		for (int i = 0; i < 6; i++) 
		{
			String xMes=listaMes[i];
			try 
			{
				JsonReader lector=new JsonReader(new FileReader("."+File.separator+"data"+File.separator+"Moving_Violations_Issued_in_"+xMes+ "_2018.json"));

				ArregloDinamico<VOMovingViolations> x=crearArregloInfo(lector);

				System.out.println("Mes cargado: "+xMes);

				for (int j = 0; j < x.darTamano(); j++) 
				{

					Integer key=x.darElemento(j).getObjectId();
					if(key != null)	arbol.put(key, x.darElemento(j));

				}
				System.out.println("Numero de infracciones del mes: "+x.darTamano());
				numDatos+=x.darTamano();
				lector.close();
			}
			catch (Exception e) 
			{
				// TODO: handle exception
				e.printStackTrace();
			}	
		}

		System.out.println("Numero de llaves(infracciones) cargadas en el arbol: "+arbol.size());

	}

	private ArregloDinamico<VOMovingViolations> crearArregloInfo(JsonReader reader) throws IOException
	{
		ArregloDinamico<VOMovingViolations> arreglo=new ArregloDinamico<VOMovingViolations>(30000000);
		reader.beginArray();
		while (reader.hasNext()) 
		{
			arreglo.agregar(crearInfraccion(reader));
		}
		reader.endArray();
		return arreglo;
	}

	private VOMovingViolations crearInfraccion(JsonReader lector) throws IOException
	{
		String ObjectId="";
		String ViolationDescription="";
		String Location="";
		String TotalPaid="";
		String AccidentIndicator="";
		LocalDateTime TicketIssueDate=null;
		String ViolationCode="";
		String FineAMT="";
		Integer Street=0;
		Integer Address_Id=0;
		String pXcord="";
		String pYcord="";
		String Penalty1="";
		String Penalty2="";

		lector.beginObject();
		while (lector.hasNext())
		{
			if(lector.peek() != JsonToken.NULL)
			{
				String info = lector.nextName();
				if (info.equals("OBJECTID")) 
				{
					ObjectId= lector.nextString();	
				} 
				else if (info.equals("LOCATION")) 
				{
					Location = lector.nextString();
				} 
				else if (info.equals("XCOORD")) 
				{
					pXcord= lector.nextString();
				} 
				else if (info.equals("YCOORD")) 
				{
					pYcord= lector.nextString();
				} 
				else if (info.equals("ADDRESS_ID")) 
				{
					try 
					{
						Address_Id = lector.nextInt();
					} 
					catch (Exception e) 
					{
						// TODO: handle exception
						Address_Id=-1;
					}
					//					System.out.println("Adr::::"+Address_Id);
				}  
				else if (info.equals("STREETSEGID")) 
				{
					try 
					{
						Street = lector.nextInt();
					} 
					catch (Exception e) 
					{
						// TODO: handle exception
						Street=-1;
					}
				} 
				else if (info.equals("VIOLATIONDESC")) 
				{
					ViolationDescription = lector.nextString();
				} 
				else if (info.equals("TOTALPAID")) 
				{
					TotalPaid = lector.nextString();
				} 
				else if (info.equals("ACCIDENTINDICATOR")) 
				{
					AccidentIndicator = lector.nextString();
				}
				else if (info.equals("TICKETISSUEDATE")) 
				{
					TicketIssueDate = convertirFecha_Hora_LDT(lector.nextString());
				}
				else if (info.equals("VIOLATIONCODE")) 
				{
					ViolationCode = lector.nextString();
				}
				else if (info.equals("FINEAMT")) 
				{
					FineAMT = lector.nextString();
				}
				else if (info.equals("PENALTY1")) 
				{
					Penalty1 = lector.nextString();
				}
				else if (info.equals("PENALTY2")) 
				{
					try 
					{
						Penalty2 = lector.nextString();
					} 
					catch (Exception e) 
					{
						// TODO: handle exception
						Penalty2="";
					}
				}
				else 
				{
					lector.skipValue();
				}
			}
			else
			{
				lector.skipValue();
			}
		}
		lector.endObject();
		VOMovingViolations infraccion= new VOMovingViolations(ObjectId, ViolationDescription, Location, TotalPaid, AccidentIndicator, TicketIssueDate, ViolationCode, FineAMT, Street, Address_Id, Penalty1, Penalty2, pXcord, pYcord);
		return infraccion;

	} 
	/**
	 * Busca y retorna del arbol la infraccion con el mismo object id pasado por parametro
	 * @return infraccion con objectId pasado por parametro, si no lo encuentra retorna null.
	 */
	public VOMovingViolations darInfraccionPorObjectId(Integer objectId)
	{
		VOMovingViolations rta=arbol.get(objectId);
		return rta;
	}
	/**
	 * Imprime en consola todos los objects id dentro de un rango dado por parametro.
	 */
	public void imprimirPorRangoObjectsId(Integer ObjectIdMenor,Integer ObjectIdMayor)
	{
		if(ObjectIdMenor>ObjectIdMayor)
		{
			System.out.println("El objectId menor es mayor al objectId mayor!!");
		}
		Iterable<Integer> x=arbol.keysInRange(ObjectIdMenor, ObjectIdMayor);
		IteratorQueue<Integer> it=(IteratorQueue<Integer>) x.iterator();
		while(it.hasNext())
		{
			System.out.println(arbol.get(it.next()).toString());
		}
	}
	/**
	 * Da la altura promedio del arbol black-red
	 */
	public double darAlturaPromedio()
	{
		int numNodos=0;
		double alturaAcum=0;
		int numNodos1=0;
		int numNodos2=0;
		int numNodos20=0;
		Iterable<Integer> x=arbol.keys();
		IteratorQueue<Integer> it=(IteratorQueue<Integer>) x.iterator();

		while(it.hasNext())
		{
			int altura=arbol.getHeight(it.next());
			alturaAcum+=altura;
			numNodos++;
			
		}

		double promedio=alturaAcum/numNodos;
		return promedio;
	}
	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha);
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}

}
