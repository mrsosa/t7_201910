package model.data_structures;

import java.lang.reflect.Array;

/**
 * 2019-01-23
 * Estructura de Datos Arreglo Dinamico de Strings.
 * El arreglo al llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * @author Fernando De la Rosa
 *
 */
public class ArregloDinamico<T extends Comparable<T>> implements IArregloDinamico<T> 
{
		/**
		 * Capacidad maxima del arreglo
		 */
        private int tamanoMax;
		/**
		 * Numero de elementos en el arreglo (de forma compacta desde la posicion 0)
		 */
        private int tamanoAct;
        /**
         * Arreglo de elementos de tamaNo maximo
         */
        private T elementos[ ];

        /**
         * Construir un arreglo con la capacidad maxima inicial.
         * @param max Capacidad maxima inicial
         */
		public ArregloDinamico( int max )
        {
               
               tamanoMax = max;
               elementos = (T[]) new Comparable[max];       
               tamanoAct = 0;
        }
        
		public void agregar( T dato )
        {
               if ( tamanoAct == tamanoMax )
               {  // caso de arreglo lleno (aumentar tamaNo)
                    tamanoMax = 2 * tamanoMax;
                    T [ ] copia = elementos;
                    elementos = (T[]) new Comparable[tamanoMax];  
                    for ( int i = 0; i < tamanoAct; i++)
                    {
                     	 elementos[i] = copia[i];
                    } 
//            	    System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
               }
               
               elementos[tamanoAct++] = dato;
       }
		
		public void agregarHeap( T dato )
        {
               if ( tamanoAct+1 == tamanoMax )
               {  // caso de arreglo lleno (aumentar tamaNo)
                    tamanoMax = 2 * tamanoMax;
                    T [ ] copia = elementos;
                    elementos = (T[]) new Comparable[tamanoMax]; 
                    for ( int i = 0; i < tamanoAct; i++)
                    {
                     	 elementos[i] = copia[i];
                    } 
//            	    System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
               }
               
               //IMPORTANTE VERSION HEAP:: ++N (le suma uno debido a que no puede empezar en 0)
               elementos[++tamanoAct] = dato;
               
       }

		public int darTamano() 
		{
			// TODO implementar
			return tamanoAct;
		}

		public T darElemento(int i) 
		{
			// TODO implementar
			return elementos[i];
		}

		public T buscar(T dato) 
		{
			// TODO implementar
			// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
			T rta=null;	
			for (int i = 0; i < elementos.length&&rta==null; i++) 
			{
				if(elementos[i]!=null&&elementos[i].compareTo(dato)==0)
				{
					rta=elementos[i];
				}
			}	
			return rta;
		}

		public T eliminar(T dato) 
		{
			// TODO implementar
			// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
			T eliminado=null;
			for (int i = 0; i < elementos.length; i++) 
			{
				if(elementos[i]!=null&&elementos[i].compareTo(dato)==0)
				{
					eliminado=elementos[i];
					elementos[i]=null;
					for (int j = i+1; j < elementos.length; j++) 
					{
						elementos[i]=elementos[j];
						i++;
					}
					elementos[elementos.length-1]=null;
					tamanoAct--;
				}
			}	
			
			return eliminado;
		}
		
		/**
		 * Elimina un dato del arreglo por el index que le pasan OJO: ESTE METODO NO AGRUPA LOS ELEMENTOS.
		 */
		public void eliminarPorPosicion(int index)
		{
		   elementos[index]=null;	
		}
		
		/**
		 * Intercambiar los datos de las posicion i y j
		 * @param datos contenedor de datos
		 * @param i posicion del 1er elemento a intercambiar
		 * @param j posicion del 2o elemento a intercambiar
		 */
		public void exchange( int i, int j)
		{
			// TODO implementar
			T tmp = elementos[i]; 
			elementos[i] = elementos[j]; 
			elementos[j] = tmp;
		}
		
}
