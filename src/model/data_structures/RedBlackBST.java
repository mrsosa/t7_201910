package model.data_structures;

import java.util.NoSuchElementException;

/**
 * Clase que representa un BST balanceado rojo-negro 
 * @author mrSosa
 * @param <K> Tipo generico de la llave del arbol
 * @param <V> Tipo generico del valor de las llaves del arbol
 * Referencias Codigo:: https://algs4.cs.princeton.edu/home/ (Pagina oficial del libro Algorithms, 4th Edition)
 * Adaptado de: https://algs4.cs.princeton.edu/33balanced/RedBlackBST.java.html
 */
public class RedBlackBST<K extends Comparable<K>,V> {
	/**
	 * Constante que asigna al color rojo true.
	 */
	private static final boolean RED   = true;
	/**
	 * Constante que asigna al color negro false
	 */
	private static final boolean BLACK = false;
	/**
	 * Raiz del nodo arbol
	 */
	private NodoArbol raiz;
	/**
	 * Subclase que representa un nodo del arbol black-red
	 */
	private class NodoArbol 
	{
		private K key;           // llave
		private V value;         // valor
		private NodoArbol left, right;  //Nodo derecha-izquierda
		private boolean color;     // color del padre = negro o hermano = rojo
		private int N;          // numero de subnodos
		//Contructor de la sub-clase
		public NodoArbol(K key, V val, int n, boolean color) 
		{
			this.key = key;
			this.value = val;
			this.color = color;
			this.N = n;
		}
	}
	/**
	 * Contrucotr el arbol.
	 */
	public RedBlackBST()
	{
	}

	/***************************************************************************
	 *  Metodos de ayuda a los nodos.
	 ***************************************************************************/
	/**
	 * El nodo x es rojo?
	 * @param x Nodo
	 * @return true si es rojo, false en caso contrario.
	 */
	private boolean isRed(NodoArbol x){
		if(x == null) return false;
		return x.color == RED;
	}

	/**
	 * Tama�o del arbol.
	 * @return tama�o
	 */
	public int size(){
		return size(raiz);
	}
	/**
	 * Metodo que retorna el tama�o del subarbol pasado por parametro
	 * @param x
	 * @return tama�o del subarbol
	 */
	private int size(NodoArbol x){
		if(x == null) return 0;
		return x.N;
	}

	/**
	 * Esta vacio el arbol?
	 * @return true si esta vacio - false en caso contrario.
	 */
	public boolean isEmpty(){
		return raiz == null;
	}

	/***************************************************************************
	 *  Metodos standard de busqueda.
	 ***************************************************************************/

	/**
	 * Dar valor segun la llave.
	 * @param key llave
	 * @return valor segun la llave.
	 */
	public V get(K key){
		if (key == null) throw new IllegalArgumentException("Lallave a buscar es null");
		return get(raiz, key);
	}
	/**
	 * Busca el valor contenido en una llave a partir de un nodo inicial pasado por parametro
	 * @param x nodoArbol por empezar la busqueda(subarbol)
	 * @param key llave a buscar
	 * @return Valor de la llave a buscar
	 */
	private V get(NodoArbol x, K key){
		while(x != null){
			int cmp = key.compareTo(x.key);
			if      (cmp < 0) x = x.left;
			else if (cmp > 0) x = x.right;
			else              return x.value;
		}
		return null;
	}
	
	/**
	 * Busca el nodoArbol con la llave pasada por parametro a partir de otro nodo inicial pasado por parametro
	 * @param x nodoArbol por empezar la busqueda(subarbol)
	 * @param key llave a buscar
	 * @return Nodo con la llave pasada por parametro 
	 */
	private NodoArbol getNodoArbol(NodoArbol x, K key){
		while(x != null){
			int cmp = key.compareTo(x.key);
			if      (cmp < 0) x = x.left;
			else if (cmp > 0) x = x.right;
			else              return x;
		}
		return null;
	}

	/**
	 * Contiene la llave?
	 * @param key Llave
	 * @return True si existe en el arbol la llave - false en caso contrario.
	 * @throws Exception El arbol esta vacio
	 */
	public boolean contains(K key) {
		return get(key) != null;
	}

	/***************************************************************************
	 *  Metodos standard de insercion.
	 ***************************************************************************/

	/**
	 * Agrega un nuevo elemento al arbol.
	 * @param key Llave
	 * @param value Valor
	 */
	public void put(K key, V value){
		if (key == null) throw new IllegalArgumentException("La llave a eliminar es null");
		if (value == null) {
			delete(key);
			return;
		}

		raiz = put(raiz, key, value);
		raiz.color = BLACK;
	}

	private NodoArbol put(NodoArbol h, K key, V value){
		if(h == null) return new NodoArbol(key, value, 1, RED);

		int cmp = key.compareTo(h.key);
		if	   (cmp < 0) h.left = put(h.left, key, value);
		else if(cmp > 0) h.right = put(h.right, key, value);
		else h.value = value;

		if(isRed(h.right) && !isRed(h.left))  	h = rotateLeft(h);
		if(isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		if(isRed(h.left) && isRed(h.right))  flipColors(h);

		h.N = size(h.left) + size(h.right) + 1;
		return h;
	}

	/***************************************************************************
	 *  Metodos para eliminar.
	 ***************************************************************************/
	/**
	 * Elimina el menor nodo del arbol rojo-negro.
	 */
	public void deleteMin(){
		if (isEmpty()) throw new NoSuchElementException("BST Est� vacio");

		if (!isRed(raiz.left) && !isRed(raiz.right)) raiz.color = RED;

		raiz = deleteMin(raiz);
		if(!isEmpty()) raiz.color = BLACK;
	}
	/**
	 * Elimina el nodo del menor arbol pasado por parametro 
	 */
	private NodoArbol deleteMin(NodoArbol h) 
	{ 
		if (h.left == null)
			return null;

		if (!isRed(h.left) && !isRed(h.left.left))
			h = moveRedLeft(h);

		h.left = deleteMin(h.left);
		return balance(h);
	}

	/**
	 * Elimina el mayor nodo del arbol rojo-negro.
	 */
	public void deleteMax()
	{
		if (isEmpty()) throw new NoSuchElementException("BST Est� vacio");

		if (!isRed(raiz.left) && !isRed(raiz.right)) raiz.color = RED;

		raiz = deleteMax(raiz);
		if(!isEmpty()) raiz.color = BLACK;
	}
	/**
	 * Elimina el mayor a partir de un nodo pasado por parametro.
	 * @param h
	 * @return n
	 */
	private NodoArbol deleteMax(NodoArbol h) { 
		if (isRed(h.left)) h = rotateRight(h);

		if (h.right == null) return null;

		if (!isRed(h.right) && !isRed(h.right.left)) h = moveRedRight(h);

		h.right = deleteMax(h.right);

		return balance(h);
	}

	/**
	 * Elimina el elemento de acuerdo a la llave. 
	 * @param key Llave
	 */
	public void delete(K key){
		if (key == null) throw new IllegalArgumentException("La llave a eliminar es null");
		if (!contains(key)) return;

		if (!isRed(raiz.left) && !isRed(raiz.right)) raiz.color = RED;

		raiz = delete(raiz, key);
		if(!isEmpty()) raiz.color = BLACK;
	}


	private NodoArbol delete(NodoArbol h, K key){
		if (key.compareTo(h.key) < 0)  {
			if (!isRed(h.left) && !isRed(h.left.left)) h = moveRedLeft(h);
			h.left = delete(h.left, key);
		}
		else {
			if (isRed(h.left))
				h = rotateRight(h);
			if (key.compareTo(h.key) == 0 && (h.right == null))
				return null;
			if (!isRed(h.right) && !isRed(h.right.left))
				h = moveRedRight(h);
			if (key.compareTo(h.key) == 0) {
				NodoArbol x = min(h.right);
				h.key = x.key;
				h.value = x.value;
				h.right = deleteMin(h.right);
			}
			else h.right = delete(h.right, key);
		}
		return balance(h);
	}

	/***************************************************************************
	 *  Metodos de apoyo para el arbol.
	 ***************************************************************************/

	private NodoArbol rotateLeft(NodoArbol h){
		NodoArbol  x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left)+ size(h.right);
		return x;		
	}

	private NodoArbol rotateRight(NodoArbol h){
		NodoArbol x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left)+ size(h.right);
		return x;
	}

	private void flipColors(NodoArbol h){
		h.color = !h.color;
		h.left.color = !h.left.color;
		h.right.color = !h.right.color;
	}

	private NodoArbol moveRedLeft(NodoArbol h) {
		flipColors(h);
		if (isRed(h.right.left)) { 
			h.right = rotateRight(h.right);
			h = rotateLeft(h);
			flipColors(h);
		}
		return h;
	}

	private NodoArbol moveRedRight(NodoArbol h) {
		flipColors(h);
		if (isRed(h.left.left)) { 
			h = rotateRight(h);
			flipColors(h);
		}
		return h;
	}

	private NodoArbol balance(NodoArbol h) {
		if (isRed(h.right))                      h = rotateLeft(h);
		if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		if (isRed(h.left) && isRed(h.right))     flipColors(h);

		h.N = size(h.left) + size(h.right) + 1;
		return h;
	}

	/***************************************************************************
	 *  Metodos utiles del arbol.
	 ***************************************************************************/
	/**
     * Retorna la altura REAL del arbol (como si fuera un  BST)).
     * @return altura real del arbol 
     */
	public int height() {
		return getHeight(raiz);
	}
	/**
	 * Calcula la altura del arbol a partir de una llave pasada por parametro 
	 */
	public int getHeight(K key)
	{
		NodoArbol buscado=getNodoArbol(raiz, key);
		int rta=getHeight(buscado);
		return rta;
	}
	
	/**
	 * Obtiene la altura real del arbol desde un nodo dado por parametro 
	 * @param x
	 * @return altura real del arbol desde un nodo dado por parametro 
	 */
	public int getHeight(NodoArbol x) {
		if (x == null) return -1;
		return 1 + Math.max(getHeight(x.left), getHeight(x.right));
	}
	
	/***************************************************************************
	 *  Metodos extras del arbol.
	 ***************************************************************************/

	/**
	 * Metodo que retorna la llave menor
	 * @return La llave menor.
	 */
	public K min() {
		if (isEmpty()) throw new NoSuchElementException("El arbol est� vacio");
		return (K) min(raiz).key;
	} 

	private NodoArbol min(NodoArbol x) { 
		if (x.left == null) return x; 
		else return min(x.left); 
	} 

	/**
	 * Metodo que retorna la llave mayor.
	 * @return La llave mayor
	 */
	public K max(){
		if (isEmpty()) throw new NoSuchElementException("El arbol est� vacio");
		return (K) max(raiz).key;
	} 

	private NodoArbol max(NodoArbol x) { 
		if (x.right == null) return x; 
		else return max(x.right); 
	} 

	/**
	 * LLave del piso
	 * @param key llave
	 * @return llave del piso
	 */
	public K floor(K key){
		if (key == null) throw new IllegalArgumentException("La llave es null");
		if (isEmpty()) throw new NoSuchElementException("El arbol est� vacio");
		NodoArbol x = floor(raiz, key);
		if (x == null) return null;
		else           return (K) x.key;
	}  

	private NodoArbol floor(NodoArbol x, K key) {
		if (x == null) return null;
		int cmp = key.compareTo(x.key);
		if (cmp == 0) return x;
		if (cmp < 0)  return floor(x.left, key);
		NodoArbol t = floor(x.right, key);
		if (t != null) return t; 
		else           return x;
	}

	public K ceiling(K key) {
		if (key == null) throw new IllegalArgumentException("La llave es null");
		if (isEmpty()) throw new NoSuchElementException("El arbol est� vacio");
		NodoArbol x = ceiling(raiz, key);
		if (x == null) return null;
		else           return x.key;  
	}

	private NodoArbol ceiling(NodoArbol x, K key) {  
		if (x == null) return null;
		int cmp = key.compareTo(x.key);
		if (cmp == 0) return x;
		if (cmp > 0)  return ceiling(x.right, key);
		NodoArbol t = ceiling(x.left, key);
		if (t != null) return t; 
		else           return x;
	}

	public K select(int k) {
		if (k < 0 || k >= size()) {
			throw new IllegalArgumentException("El argumento a seleccionar es invalido: " + k);
		}
		NodoArbol x = select(raiz, k);
		return (K) x.key;
	}

	private NodoArbol select(NodoArbol x, int k) {
		int t = size(x.left); 
		if      (t > k) return select(x.left,  k); 
		else if (t < k) return select(x.right, k-t-1); 
		else            return x; 
	} 

	public int rank(K key) {
		if (key == null) throw new IllegalArgumentException("La llave es null");
		return rank(key, raiz);
	} 

	private int rank(K key, NodoArbol x) {
		if (x == null) return 0; 
		int cmp = key.compareTo((K) x.key); 
		if      (cmp < 0) return rank(key, x.left); 
		else if (cmp > 0) return 1 + size(x.left) + rank(key, x.right); 
		else              return size(x.left); 
	} 

	/***************************************************************************
	 *  Otros.
	 ***************************************************************************/
	/**
	 * Retorna todas llaves del �rbol como un iterador.
	 * @return iterador con todas las llaves del arbol.
	 */
	public Iterable<K> keys()
	{
		if (isEmpty()) return new Queue<K>();
		return keysInRange(min(), max());
	}
	/**
	 * Retorna una cola con todas las llaves K en el �rbol que se encuentran en el rango de llaves dado.Por eficiencia, debe intentarse No recorrer todo el �rbol.
	 * @param lo llave inicial
	 * @param hi llave final
	 * @return iterador con las llaves dentro del rango
	 */
	public Iterable<K> keysInRange(K lo, K hi) 
	{
		if (lo == null) throw new IllegalArgumentException("La llave menor es null");
		if (hi == null) throw new IllegalArgumentException("La llave mayor es null");

		Queue<K> queue = new Queue<K>();
		keys(raiz, queue, lo, hi);
		return queue;
	} 

	private void keys(NodoArbol x, Queue<K> queue, K lo, K hi) 
	{ 
		if (x == null) return; 
		int cmplo = lo.compareTo(x.key); 
		int cmphi = hi.compareTo(x.key); 
		if (cmplo < 0) keys(x.left, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key); 
		if (cmphi > 0) keys(x.right, queue, lo, hi); 
	}

	/**
	 * Retorna una cola todos los valores V en el �rbol que est�n asociados al rango de llaves dado. Por eficiencia, debe intentarse No recorrer todo el �rbol
	 * @param lo llave inicial
	 * @param hi llave final
	 * @return iterador con todos los valores 
	 */
	public Iterable<V> valueInRange(K lo, K hi){
		if (lo == null) throw new IllegalArgumentException("La llave menor es null");
		if (hi == null) throw new IllegalArgumentException("La llave mayor es null");

		Queue<V> queue = new Queue<V>();
		valueInRange(raiz, queue, lo, hi);
		return queue;
	}

	private void valueInRange(NodoArbol x, Queue<V> queue, K lo, K hi) {
		if(x == null) return;
		int cmplo = lo.compareTo(x.key);
		int cmphi = hi.compareTo(x.key); 
		if (cmplo < 0) valueInRange(x.left, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.value); 
		if (cmphi > 0) valueInRange(x.right, queue, lo, hi);
	}

	public int size(K lo, K hi)
	{
		if (lo == null) throw new IllegalArgumentException("La llave menor es null");
		if (hi == null) throw new IllegalArgumentException("La llave mayor es null");
		if (lo.compareTo(hi) > 0) return 0;
		if (contains(hi)) return rank(hi) - rank(lo) + 1;
		else              return rank(hi) - rank(lo);
	}

	/***************************************************************************
	 *  Verificar la definicion de un arbol Rojo Negro.
	 ***************************************************************************/
	public boolean check() {
		if (!isBST())            System.out.println("Not in symmetric order");
		if (!isSizeConsistent()) System.out.println("Subtree counts not consistent");
		if (!isRankConsistent()) System.out.println("Ranks not consistent");
		if (!is23())             System.out.println("Not a 2-3 tree");
		if (!isBalanced())       System.out.println("Not balanced");
		return isBST() && isSizeConsistent() && isRankConsistent() && is23() && isBalanced();
	}

	// �Este �rbol binario satisface un orden sim�trico?
	// Nota: esta prueba tambi�n garantiza que la estructura de datos es un �rbol binario ya que el orden es estricto
	private boolean isBST() {
		return isBST(raiz, null, null);
	}

	// es el �rbol arraigado en x un BST con todas las claves estrictamente entre m�nimo y m�ximo
	// (si min o max es nulo, tratar como una restricci�n vac�a)
	// Cr�dito: la elegante soluci�n de Bob Dondero7
	private boolean isBST(NodoArbol x, K min, K max) {
		if (x == null) return true;
		if (min != null && ((Comparable<K>) x.key).compareTo(min) <= 0) return false;
		if (max != null && ((Comparable<K>) x.key).compareTo(max) >= 0) return false;
		return isBST(x.left, min, x.key) && isBST(x.right, x.key, max);
	} 

	// Son los tama�os de los campos correctos?
	private boolean isSizeConsistent() { return isSizeConsistent(raiz); }
	private boolean isSizeConsistent(NodoArbol x) {
		if (x == null) return true;
		if (x.N != size(x.left) + size(x.right) + 1) return false;
		return isSizeConsistent(x.left) && isSizeConsistent(x.right);
	} 

	// revisa que los rangos sean consistentes
	private boolean isRankConsistent(){
		for (int i = 0; i < size(); i++)
			if (i != rank(select(i))) return false;
		for (K key : keys())
			if (key.compareTo(select(rank(key))) != 0) return false;
		return true;
	}

	//�El �rbol no tiene enlaces rojos a la derecha y, como m�ximo, uno (izquierda)
	// enlaces rojos en una fila en cualquier camino?
	private boolean is23() { return is23(raiz); }
	private boolean is23(NodoArbol x) {
		if (x == null) return true;
		if (isRed(x.right)) return false;
		if (x != raiz && isRed(x) && isRed(x.left))
			return false;
		return is23(x.left) && is23(x.right);
	} 

	//�Todas las rutas de la ra�z a la hoja tienen el mismo n�mero de enlaces negros??
	private boolean isBalanced() { 
		int black = 0;     //n�mero de enlaces negros en la ruta desde la ra�z a min
		NodoArbol x = raiz;
		while (x != null) {
			if (!isRed(x)) black++;
			x = x.left;
		}
		return isBalanced(raiz, black);
	}

	//�Cada ruta desde la ra�z a una hoja tiene el n�mero dado de enlaces negros?
	private boolean isBalanced(NodoArbol x, int black) {
		if (x == null) return black == 0;
		if (!isRed(x)) black--;
		return isBalanced(x.left, black) && isBalanced(x.right, black);
	}
}
