package view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IQueue;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el nÃºmero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;
	/**
	 * Menu del view
	 */
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 7----------------------");
		System.out.println("1. Cargar el semestre de infracciones.");
		System.out.println("2. Consultar informacion asociada a un ObjectId.");
		System.out.println("3. Consultar ObjectId por un rango dado.");
	    System.out.println("4. Salir ");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	/**
	 * Imprime todos los datos de un arreglo comparable pasado por parametro
	 * @param muestra
	 */
	public void printDatosMuestra(  Comparable [ ] muestra)
	{
		for ( Comparable elemento : muestra)
		{	
			if(elemento!=null)
			System.out.println(  elemento.toString() );    
		}
	}
	/**
	 * Imprime un mensaje por consola
	 * @param mensaje
	 */
	public void printMessage(String mensaje) 
	{
		System.out.println(mensaje);
	}
	
}
