Nombres: Manuel Sosa (201815393), Nicol�s Tobo (201817465)  

Analisis del arbol red-black construido

a. Total de nodos en el arbol: 599207 nodos (donde cada nodo guarda una infracci�n.)
b. Altura real del arbol red-black: 26
c. Altura promedio del arbol red-blank: 23.980380736540127
d. Si tomamos el peor caso del arbol red-black (en el que cada nodo tiene un enlace rojo(tipo 3)), su altura seria dada por: O(2(logN))(Donde log es en base 2)
   entonces: 2log2(599207)= 38.385
e. Si tomamos el mejor caso del arbol red-black (en el que cada nodo no tiene enlaces rojos (tipo2)), su altura seria dada por: O(logN)(Donde log es en base 2)
   entonces: log2(599207)=19.193
f. Si tomamos el peor caso del arbol 2-3 (en el que cada nodo es tipo 2), su altura seria dada por: O(logN)(Donde log es en base 2)
   entonces: log2(599207)=19.193
g. Si tomamos el mejor caso del arbol 2-3 (en el que cada nodo es tipo 3), su altura seria dada por: O(logN)(Donde log es en base 3)
   entonces: log3(599207)=12.109
h. Comparando la altura real del arbol con respecto a la teoria de los arboles black-red, la altura real es mayor al mejor caso (19.19) y menor al peor caso (38.85)
de estos arboles, esto quiere decir que el arbol esta en un caso intermedio donde hay multiples nodos con uno o cero enlaces rojos. Ahora si comparamos la altura 
con la teoria del arbol 2-3, en el mejor(12.10) y peor caso(19.123) es mayor la altura , debido a que en la realidad un nodo 3 puede albergar mas informacion, 
optimizando asi el no tener que hacer un enlace extra de color rojo.
i. Comparando la altura promedio del arbol con respecto a la teoria de los arboles black-red, la altura promedio es mayor al mejor caso (19.19) y menor al peor caso (38.85)
de estos arboles, esto quiere decir que en promedio la busqueda de una infraccion en el arbol esta rondando por el nivel 23-24. Ahora si comparamos la altura promedio 
con la teoria del arbol 2-3, en el mejor(12.10) y peor caso(19.123) es mayor la altura promedio, demostrando que estos arboles son mejores en la busqueda de una llave. 